#![allow(dead_code)]
#![allow(unused_imports)]
#![allow(clippy::empty_loop)]
#![no_main]
#![no_std]
#![feature(type_alias_impl_trait)]

// Halt on panic
use panic_halt as _; // panic handler

use stm32f4xx_hal::gpio::{Output, PushPull};
use stm32f4xx_hal::gpio::gpioc::PC13;
use stm32f4xx_hal::otg_fs::{UsbBusType, USB};
use stm32f4xx_hal::prelude::*;
use stm32f4xx_hal::pac::TIM2;
use stm32f4xx_hal::timer::MonoTimerUs;
use usb_device::bus::UsbBusAllocator;
use usb_device::prelude::*;

#[rtic::app(device = stm32f4xx_hal::pac, peripherals = true, dispatchers = [USART1])]
mod app {
    use super::*;
    use usb_device::class::UsbClass; // Needed for keyboard.poll() and mouse.poll() to work

    #[shared]
    struct Shared {
        usb_dev: usb_device::device::UsbDevice<'static, UsbBusType>,
        usb_class: keyberon::Class<'static, UsbBusType, ()>,
    }

    #[local]
    struct Local {
        led: PC13<Output<PushPull>>,
        state: bool
    }

    #[monotonic(binds = TIM2, default = true)]
    type MicrosecMono = MonoTimerUs<TIM2>;

    #[init(local = [
        USB_BUS: Option<UsbBusAllocator<UsbBusType>> = None,
        EP_MEMORY: [u32; 1024] = [0; 1024]
    ])]
    fn init(cx: init::Context) -> (Shared, Local, init::Monotonics) {
        // Setup GPIO.
        let gpioa = cx.device.GPIOA.split();
        let gpiob = cx.device.GPIOB.split();
        let gpioc = cx.device.GPIOC.split();

        // Setup clocks
        let rcc = cx.device.RCC.constrain();
        let clocks = rcc
            .cfgr
            .use_hse(25.MHz())
            .sysclk(84.kHz())
            .require_pll48clk()
            .freeze();

        // Configure TIM2 as clock source.
        let mono = cx.device.TIM2.monotonic(&clocks);

        let mut led = gpioc.pc13.into_push_pull_output();
        led.set_high();

        let usb = USB {
            usb_global: cx.device.OTG_FS_GLOBAL,
            usb_device: cx.device.OTG_FS_DEVICE,
            usb_pwrclk: cx.device.OTG_FS_PWRCLK,
            pin_dm: stm32f4xx_hal::gpio::alt::otg_fs::Dm::PA11(gpioa.pa11.into_alternate()),
            pin_dp: stm32f4xx_hal::gpio::alt::otg_fs::Dp::PA12(gpioa.pa12.into_alternate()),
            hclk: clocks.hclk(),
        };
        *cx.local.USB_BUS = Some(UsbBusType::new(usb, cx.local.EP_MEMORY));
        let usb_bus = cx.local.USB_BUS.as_ref().unwrap();

        let usb_class = keyberon::new_class(usb_bus, ());
        let usb_dev = keyberon::new_device(usb_bus);


        tick::spawn_after(1000.millis()).ok();

        (
            Shared {
                usb_dev,
                usb_class,
            },
            Local {
                led,
                state: true
            },
            init::Monotonics(mono)
        )
    }

    #[task(binds = OTG_FS, priority = 3, shared = [usb_dev, usb_class])]
    fn usb_tx(cx: usb_tx::Context) {
        (cx.shared.usb_dev, cx.shared.usb_class).lock(|usb_dev, usb_class| {
            if usb_dev.poll(&mut [usb_class]) {
                usb_class.poll();
            }
        });
    }

    #[task(binds = OTG_FS_WKUP, priority = 3, shared = [usb_dev, usb_class])]
    fn usb_rx(cx: usb_rx::Context) {
        (cx.shared.usb_dev, cx.shared.usb_class).lock(|usb_dev, usb_class| {
            if usb_dev.poll(&mut [usb_class]) {
                usb_class.poll();
            }
        });
    }

    #[task(priority = 2,
        local = [ led, state ]
    )]
    fn tick(mut cx: tick::Context) {
        tick::spawn_after(1000.millis()).ok();
        if *cx.local.state {
            cx.local.led.set_high();
            *cx.local.state = false;
        } else {
            cx.local.led.set_low();
            *cx.local.state = true;
        }
    }
}
