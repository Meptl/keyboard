#![allow(dead_code)]
#![allow(unused_imports)]
#![allow(clippy::empty_loop)]
#![no_main]
#![no_std]
#![feature(type_alias_impl_trait)]

// Halt on panic
use panic_halt as _; // panic handler

use core::ops::Div;
use analog_multiplexer::{DummyPin, Multiplexer};
use keyberon::key_code::KbHidReport;
use stm32f4xx_hal::gpio::{Analog, Output, PushPull};
use stm32f4xx_hal::gpio::gpioa::PA0;
use stm32f4xx_hal::gpio::gpioc::PC13;
use stm32f4xx_hal::adc::{config::Clock, config::AdcConfig, config::SampleTime, Adc};
use stm32f4xx_hal::otg_fs::{UsbBusType, USB};
use stm32f4xx_hal::prelude::*;
use stm32f4xx_hal::pac::{ADC1, TIM2};
use stm32f4xx_hal::timer::MonoTimerUs;
use stm32f4xx_hal::timer;
use usb_device::bus::UsbBusAllocator;
use usb_device::prelude::*;
use keyberon::layout::{Event, Layout};

mod aliases;
mod config;
mod config_structs;
mod layout;
mod multiplexers;

const MV_DIVISOR: u16 = 4; // A cheap way to deal with voltage wobble/average things out
                           // FYI: Setting the divisor to 4 should give us 80-100 "steps" of resolution per sensor

pub struct Leds {
    num_lock: PC13<Output<PushPull>>,
}
impl keyberon::keyboard::Leds for Leds {
    fn num_lock(&mut self, status: bool) {
        if status {
            self.num_lock.set_low();
        } else {
            self.num_lock.set_high();
        }
    }
}

#[rtic::app(device = stm32f4xx_hal::pac, peripherals = true, dispatchers = [USART1])]
mod app {
    use super::*;
    use usb_device::class::UsbClass; // Needed for keyboard.poll() and mouse.poll() to work

    #[shared]
    struct Shared {
        config: config_structs::Config,
        usb_dev: usb_device::device::UsbDevice<'static, UsbBusType>,
        usb_class: keyberon::Class<'static, UsbBusType, ()>,
    }

    #[local]
    struct Local {
        multiplexer: aliases::Multiplex,
        adc: Adc<ADC1>,
        layout: Layout<4, 4, 1, ()>,
        mx_com: PA0<Analog>,
        mx_com_states: multiplexers::ChannelStates,
        led: PC13<Output<PushPull>>,
        state: bool
    }

    #[monotonic(binds = TIM2, default = true)]
    type MicrosecMono = MonoTimerUs<TIM2>;

    #[init(local = [
        USB_BUS: Option<UsbBusAllocator<UsbBusType>> = None,
        EP_MEMORY: [u32; 1024] = [0; 1024]
    ])]
    fn init(cx: init::Context) -> (Shared, Local, init::Monotonics) {
        // Pull config.
        // Based on SYSCLK frequency. Converts seconds to cycles.
        let recalibration_refresh: u32 = 84_000_000 * config::KEYBOARD_RECALIBRATION_RATE;

        // TODO: Write a macro that loads these automatically
        let keyboard_config = config_structs::KeyboardConfig {
            north_down: config::KEYBOARD_NORTH_DOWN,
            actuation_threshold: config::KEYBOARD_ACTUATION_THRESHOLD,
            release_threshold: config::KEYBOARD_RELEASE_THRESHOLD,
            ignore_below: config::KEYBOARD_IGNORE_BELOW,
            recalibration_rate: recalibration_refresh,
            num_multiplexers: config::KEYBOARD_NUM_MULTIPLEXERS,
            max_channels: config::KEYBOARD_MAX_CHANNELS,
            usb_vid: config::KEYBOARD_USB_VID,
            usb_pid: config::KEYBOARD_USB_PID,
        };
        let dev_config = config_structs::DevConfig {
            debug_refresh_interval: config::DEV_DEBUG_REFRESH_INTERVAL,
        };
        let config = config_structs::Config {
            keyboard: keyboard_config,
            dev: dev_config,
        };

        // Setup GPIO.
        let gpioa = cx.device.GPIOA.split();
        let gpiob = cx.device.GPIOB.split();
        let gpioc = cx.device.GPIOC.split();

        // Setup clocks
        let rcc = cx.device.RCC.constrain();
        let clocks = rcc
            .cfgr
            .use_hse(25.MHz())
            .sysclk(84.kHz())
            .require_pll48clk()
            .freeze();

        // Configure TIM2 as clock source.
        let mono = cx.device.TIM2.monotonic(&clocks);
        let mut delay = cx.device.TIM3.delay_ms(&clocks);
        delay.delay(1000.millis());

        // Setup multiplexer.
        // Pclk2_div_2 gives us an ADC running at 42Mhz (because sysclk is at 84Mhz)
        let adc_config = AdcConfig::default().clock(Clock::Pclk2_div_2);
        // NOTE: With a 42Mhz ADC clock and a SampleTime of Cycles_480 we'd be able
        // to read all 16 channels on all six analog multiplexers (96 pins) in about
        // 1.25ms which is just *barely* too slow for a 1000Hz USB polling rate.
        // For this reason we take the next cycle down at Cycles_28 which results in
        // a read-all-channels time of about 0.160ms. So that's the best accuracy
        // we can get without going over our 1ms budget.
        // NOTE: Formula for calcuating number of cycles: ((<cycles>+12)/42)*96
        let mut adc = Adc::adc1(cx.device.ADC1, true, adc_config);
        let mx_com = gpioa.pa0.into_analog();
        let mut mx_com_states: multiplexers::ChannelStates = Default::default();
        let s0 = gpioc.pc14.into_push_pull_output();
        let s1 = gpiob.pb8.into_push_pull_output();
        let s2 = gpiob.pb12.into_push_pull_output();
        let s3 = gpioa.pa15.into_push_pull_output();
        let en = DummyPin; // Just run it to GND to keep always-enabled
        let mut multiplexer = Multiplexer::new((s0, s1, s2, s3, en));

        // Read in the initial millivolt values for all analog channels so we have
        // a default/resting state to evaluate against.  We'll set new defaults later
        // after we've captured a few values (controlled by DEFAULT_WAIT_MS).
        for chan in 0..16 {
            // This sets the channel on all multiplexers simultaneously
            // (since they're all connected to the same S0,S1,S2,S3 pins).
            multiplexer.set_channel(chan);
            let sample = adc.convert(&mx_com, SampleTime::Cycles_28);
            let millivolts = adc.sample_to_millivolts(sample).div(MV_DIVISOR);
            mx_com_states[chan.into()].update_default(millivolts);
        }

        // Setup USB and keyberon. We'll reuse the dev boards LED for numlock.
        let mut led = gpioc.pc13.into_push_pull_output();
        led.set_high();
        // let leds = Leds { num_lock: led };

        let usb = USB {
            usb_global: cx.device.OTG_FS_GLOBAL,
            usb_device: cx.device.OTG_FS_DEVICE,
            usb_pwrclk: cx.device.OTG_FS_PWRCLK,
            pin_dm: stm32f4xx_hal::gpio::alt::otg_fs::Dm::PA11(gpioa.pa11.into_alternate()),
            pin_dp: stm32f4xx_hal::gpio::alt::otg_fs::Dp::PA12(gpioa.pa12.into_alternate()),
            hclk: clocks.hclk(), // stm32f4xx_hal version 0.9+ requires this
        };
        *cx.local.USB_BUS = Some(UsbBusType::new(usb, cx.local.EP_MEMORY));
        let usb_bus = cx.local.USB_BUS.as_ref().unwrap();

        let usb_class = keyberon::new_class(usb_bus, ());
        let usb_dev = keyberon::new_device(usb_bus);


        (
            Shared {
                config,
                usb_dev,
                usb_class,
            },
            Local {
                multiplexer,
                adc,
                layout: Layout::new(&crate::layout::LAYERS),
                mx_com,
                mx_com_states,
                led,
                state: false
            },
            init::Monotonics(mono)
        )
    }

    #[task(binds = OTG_FS, priority = 3, shared = [usb_dev, usb_class])]
    fn usb_tx(cx: usb_tx::Context) {
        (cx.shared.usb_dev, cx.shared.usb_class).lock(|usb_dev, usb_class| {
            if usb_dev.poll(&mut [usb_class]) {
                usb_class.poll();
            }
        });
    }

    #[task(binds = OTG_FS_WKUP, priority = 3, shared = [usb_dev, usb_class])]
    fn usb_rx(cx: usb_rx::Context) {
        (cx.shared.usb_dev, cx.shared.usb_class).lock(|usb_dev, usb_class| {
            if usb_dev.poll(&mut [usb_class]) {
                usb_class.poll();
            }
        });
    }

    #[task(priority = 2,
        shared = [ config, usb_class ],
        local = [ multiplexer, adc, layout, mx_com, mx_com_states, led, state ]
    )]
    fn tick(mut cx: tick::Context) {
        tick::spawn_after(1000.millis()).ok();

        let minimum_volts = cx.shared.config.lock(|config| config.keyboard.ignore_below);
        let north_down = cx.shared.config.lock(|config| config.keyboard.north_down);
        let actuation_threshold = cx.shared.config.lock(|config| config.keyboard.actuation_threshold);
        let release_threshold = cx.shared.config.lock(|config| config.keyboard.release_threshold);
        let adc = cx.local.adc;
        let multiplexer = cx.local.multiplexer;
        let layout = cx.local.layout;

        // Read in the initial millivolt values for all analog channels so we have
        // a default/resting state to evaluate against.  We'll set new defaults later
        // after we've captured a few values (controlled by DEFAULT_WAIT_MS).
        for chan in 0..16 {
            // This sets the channel on all multiplexers simultaneously
            // (since they're all connected to the same S0,S1,S2,S3 pins).
            multiplexer.set_channel(chan);
            let mut ch_state = cx.local.mx_com_states[chan.into()];
            let sample = adc.convert(cx.local.mx_com, SampleTime::Cycles_28);
            let millivolts = adc.sample_to_millivolts(sample).div(MV_DIVISOR);
            ch_state.record_value(millivolts);
            if ch_state.value <= minimum_volts {
                continue;
            }

            let voltage_difference = if millivolts < ch_state.default {
                if north_down > 0 {
                    ch_state.default - millivolts // North side down switches result in a mV drop
                } else {
                    0
                }
            } else {
                if north_down > 0 {
                    0
                } else {
                    millivolts - ch_state.default // South side down switches result in a mV increase
                }
            };

            if voltage_difference > actuation_threshold {
                if !ch_state.pressed {
                    cx.local.mx_com_states.press(chan.into());
                    let _ = layout.event(Event::Press(0, chan as u8));
                }
            } else if voltage_difference < release_threshold {
                if ch_state.pressed {
                    cx.local.mx_com_states.release(chan.into());
                    let _ = layout.event(Event::Press(0, chan as u8));
                }
            }
        }

        let report: KbHidReport = layout.keycodes().collect();
        if cx.shared.usb_class.lock(|k| k.device_mut().set_keyboard_report(report.clone())) {
            while let Ok(0) = cx.shared.usb_class.lock(|k| k.write(report.as_bytes())) {}
        }

        if *cx.local.state {
            cx.local.led.set_high();
            *cx.local.state = false;
        } else {
            cx.local.led.set_low();
            *cx.local.state = true;
        }
    }
}
