# Testing
qemu-system-arm \
  -cpu cortex-m4 \
  -machine lm3s6965evb \
  -nographic \
  -semihosting-config enable=on,target=native \
  -kernel target/thumbv7em-none-eabi/debug/keyboard

STM32F411CEU6

target extended-remote /dev/ttyACM0
monitor swdp_scan
attach 1
load target/thumbv7em-none-eabi/debug/keyboard
