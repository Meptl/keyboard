use num_format::{Buffer, CustomFormat};
/// Configuration structs; separate from config.rs so we can use them in build.rs without having to duplicate them
// use core::fmt::Write;
// use heapless::String;
// use heapless::consts::U32;
use serde::{Deserialize, Serialize};

// This lets us generate a userconfig.rs from inside build.rs that gets include!(concat!()) inside config.rs:
macro_rules! add_const_gen {
    (
        $(#[$meta:meta])*
        pub struct $struct_name:ident {
        $(
            $(#[$field_meta:meta])*
            $field_vis:vis $field_name:ident : $field_type:ty
        ),*$(,)+
        }
    ) => {
        $(#[$meta])*
        pub struct $struct_name {
            $(
                $(#[$field_meta])*
                pub $field_name : $field_type,
            )*
        }

        impl $struct_name {
            #[allow(dead_code)]
            fn field_names() -> &'static [&'static str] {
                static NAMES: &'static [&'static str] = &[$(stringify!($field_name)),*];
                NAMES
            }

            #[allow(dead_code)]
            fn gen_meta_tuple(&self, field: &'static str) -> (&str, &str, &str, Buffer) {
                let rust_format = CustomFormat::builder()
                    .separator("_")
                    .build().unwrap();
                match field {
                    $(stringify!($field_name) => {
                        let mut buf = Buffer::default();
                        buf.write_formatted(&self.$field_name, &rust_format);
                        (
                            stringify!($struct_name),
                            stringify!($field_name),
                            stringify!($field_type),
                            buf
                        )
                    }),*
                    _ => ("","","",Buffer::default())
                }
            }

            // #[allow(dead_code)]
            // fn gen_meta_tuple(&self, field: &'static str) -> (&str, &str, &str, &str) {
            //     // let rust_format = CustomFormat::builder()
            //     //     .separator("_")
            //     //     .build().unwrap();
            //     match field {
            //         $(stringify!($field_name) => {
            //             // let mut buf = Buffer::default();
            //             // buf.write_formatted(&self.$field_name, &rust_format);
            //             let mut data = String::<U32>::from("");
            //             (
            //                 stringify!($struct_name),
            //                 stringify!($field_name),
            //                 stringify!($field_type),
            //                 write!(data, "{}", &self.$field_name),
            //             )
            //         }),*
            //         _ => ("","","","")
            //     }
            // }
        }
    }
}

add_const_gen! {
/// Configuration items related to keys, sensors, and the rotary encoder
#[derive(Debug, Serialize, Deserialize)]
pub struct KeyboardConfig {
    /// Millivolts difference where we consider it a Press()
    pub north_down: u8,
    /// Millivolts difference where we consider it a Press()
    pub actuation_threshold: u16,
    /// Millivolts above the actuation threshold where we consider it a Release() (prevents bouncing)
    pub release_threshold: u16,
    /// Millivolt values below this value will be ignored (so we can skip mux pins connected to ground)
    pub ignore_below: u16,
    /// How often to check to see if the default mV values need to be adjusted (cycles)
    pub recalibration_rate: u32,
    /// Total number of multiplexers on this keyboard
    pub num_multiplexers: usize,
    /// Maximum number of channels per multiplexer or buttons per IR remote (use whatever is greater)
    pub max_channels: usize,
    /// The USB VID the keyboard will identify itself with
    pub usb_vid: u16,
    /// The USB PID the keyboard will identify itself with
    pub usb_pid: u16,
}
}

add_const_gen! {
/// Configuration items related to development stuff
#[derive(Debug, Serialize, Deserialize)]
pub struct DevConfig {
    /// Minimum amount of time to wait before sending debug messages to the debugger
    pub debug_refresh_interval: u16,
}
}

/// Central location for referencing and updating runtime settings
#[derive(Debug, Serialize, Deserialize)]
pub struct Config {
    /// Keyboard configuration items
    pub keyboard: KeyboardConfig,
    /// Development configuration items (e.g. debug stuff)
    pub dev: DevConfig,
}
