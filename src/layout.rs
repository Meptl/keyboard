use keyberon::action::{k, l, m, Action::*, HoldTapConfig};
use keyberon::key_code::KeyCode::*;

/* Keyberon was made for key switch matrices, our keyboard doesn't follow this wiring layout, so we
 * map it using this macro. Unfortunately we can't have the comma-less array definitions that
 * keyberon has because we aren't using a procedural macros.
 */
macro_rules! mux_map {
    (
        [$a:ident, $b:ident, $c:ident, $d:ident],
        [$e:ident, $f:ident, $g:ident, $h:ident],
        [$i:ident, $j:ident, $k:ident, $l:ident],
        [$m:ident, $n:ident, $o:ident, $p:ident],
    ) => {
        keyberon::layout::layout! {
            {
                [$m $i $a $k],
                [$e $b $f $c],
                [$d $g $j $h],
                [$p $l $n $o],
            }
        }
    };
}


#[rustfmt::skip]
pub static LAYERS: keyberon::layout::Layers<4, 4, 1, ()> = mux_map! {
    [Kp1, Kp2, Kp3, t],
    [Kp4, Kp5, Kp6, t],
    [Kp7, Kp8, Kp9, t],
    [t, Kp0, t, t],
};
