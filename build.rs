//! Reads our Config.toml and generates userconfig.rs by converting the values to constants
//!
//! This build script copies the `memory.x` file from the crate root into
//! a directory where the linker can always find it at build time.
//! For many projects this is optional, as the linker always searches the
//! project root directory -- wherever `Cargo.toml` is. However, if you
//! are using a workspace or have a more complicated build setup, this
//! build script becomes required. Additionally, by requesting that
//! Cargo re-run the build script whenever `memory.x` is changed,
//! updating `memory.x` ensures a rebuild of the application with the
//! new memory settings.
//!
//! The build script also sets the linker flags to tell it which link script to use.

use std::env;
use std::fs;
use std::fs::File;
use std::io::Write;
use std::path::PathBuf;

use chrono::{DateTime, Utc};

// A little hack so we can use our structs from config.rs without having to duplicate them here:
include!(concat!(env!("CARGO_MANIFEST_DIR"), "/src/config_structs.rs"));
// NOTE: config_structs.rs includes the serde import so we don't need it here

fn main() {
    // Put `memory.x` in our output directory and ensure it's
    // on the linker search path.
    let out = &PathBuf::from(env::var_os("OUT_DIR").unwrap());
    File::create(out.join("memory.x"))
        .unwrap()
        .write_all(include_bytes!("memory.x"))
        .unwrap();
    println!("cargo:rustc-link-search={}", out.display());

    // By default, Cargo will re-run a build script whenever
    // any file in the project changes. By specifying `memory.x`
    // here, we ensure the build script is only re-run when
    // `memory.x` is changed.
    println!("cargo:rerun-if-changed=memory.x");

    // Specify linker arguments.

    // `--nmagic` is required if memory section addresses are not aligned to 0x10000,
    // for example the FLASH and RAM sections in your `memory.x`.
    // See https://github.com/rust-embedded/cortex-m-quickstart/pull/95
    println!("cargo:rustc-link-arg=--nmagic");

    // Set the linker script to the one provided by cortex-m-rt.
    println!("cargo:rustc-link-arg=-Tlink.x");

    let now: DateTime<Utc> = Utc::now();
    // env::set_var("SERIALNOW", now.to_rfc3339()); // Used with the Riskeyboard firmware serial number
    println!("cargo:rustc-env=SERIALNOW={}", now.timestamp()); // Used with the Riskeyboard firmware serial number
    let dest_path = out.join("userconfig.rs");
    let mut outf = String::new();
    let contents = fs::read_to_string(concat!(env!("CARGO_MANIFEST_DIR"), "/Config.toml")).unwrap();
    let decoded: Config = toml::from_str(&contents[..]).unwrap();
    // TODO: Figure outf a way to iterate over configurable items instead of having them hard coded like this
    for fname in KeyboardConfig::field_names().iter() {
        let meta = &decoded.keyboard.gen_meta_tuple(fname);
        let const_out = format!(
            "pub const {}_{}: {} = {};\n",
            meta.0.to_uppercase().split("CONFIG").next().unwrap(),
            meta.1.to_uppercase(),
            meta.2,
            meta.3);
        outf.push_str(&const_out);
    }
    for fname in DevConfig::field_names().iter() {
        let meta = &decoded.dev.gen_meta_tuple(fname);
        let const_out = format!(
            "pub const {}_{}: {} = {};\n",
            meta.0.to_uppercase().split("CONFIG").next().unwrap(),
            meta.1.to_uppercase(),
            meta.2,
            meta.3);
        outf.push_str(&const_out);
    }
    fs::write(&dest_path, outf).unwrap();
    println!("cargo:rerun-if-changed=Config.toml");
    println!("cargo:rerun-if-changed=build.rs");
}
